from flask_login import UserMixin,LoginManager
from werkzeug.security import generate_password_hash, check_password_hash

login_manager = LoginManager()

class User(UserMixin):
    def __init__(self, id, name, email, password, typeuser):
        self.id = id
        self.name = name
        self.email = email
        self.password = password #generate_password_hash(password)
        self.typeuser = typeuser

    # Flask-Login integration
    def is_authenticated(self):
        return True

    def is_active(self): # line 37
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    def get_name(self):
        return self.name 

    def get_user(id):
        return self.name
        return self.email
        return self.typeuser       

    @login_manager.user_loader
    def load_user(id):
        return User.get_id(id)


    # def set_password(self, password):
    #     self.password = generate_password_hash(password)

    # def check_password(self, password):
    #     return check_password_hash(self.password, password)

    # def __repr__(self):
    #     return '<User {}>'.format(self.email)