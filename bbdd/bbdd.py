import sqlite3

#Crear las BBDD
def crearDataBase():

    #Conexión
    conexion=sqlite3.connect('/pruebaRaven.db')
    #crear cursor
    cursor = conexion.cursor()
   #crear una tabla usuarios
    cursor.execute("CREATE TABLE IF NOT EXISTS usuarios("+
    "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"+
    "name varchar(255),"+
    "email varchar(255),"+
    "password varchar(255),"+
    "typeuser varchar(20)"
    ")")

    #crear una tabla archivos
    cursor.execute("CREATE TABLE IF NOT EXISTS archivos("+
    "id INTEGER PRIMARY KEY AUTOINCREMENT,"+
    "id_usuario INTEGER references usuarios(id),"
    "name varchar(255),"+
    "size varchar(255),"+
    "date timestamp,"+
    "namehash varchar(255)"
    ")")


    #commitear para guardar cambios
    conexion.commit()
    #cerrar conexion
    conexion.close()

#Insertar usuario
def insertUser(userObject):
    
    #Conexión
    conexion=sqlite3.connect('/pruebaRaven.db')
    #crear cursor
    cursor = conexion.cursor()
    #insert
    cursor.execute("INSERT INTO usuarios VALUES (NULL,?,?,?,?)",userObject)
    #  commitear para guardar cambios
    conexion.commit()
    #cerrar conexion
    conexion.close()

#Insertar Archivo
def insertFile(fileObject):
    #Conexión
    conexion=sqlite3.connect('/pruebaRaven.db')
    #crear cursor
    cursor = conexion.cursor()
    #insert
    cursor.execute("INSERT INTO archivos VALUES (NULL,?,?,?,?,?)",fileObject)
    #  commitear para guardar cambios
    conexion.commit()
    #cerrar conexion
    conexion.close()

#mostrar todos los usuarios
def showAllUsers():

     #Conexión
    conexion=sqlite3.connect('/pruebaRaven.db')
    #crear cursor
    cursor = conexion.cursor()
    cursor.execute("SELECT * FROM usuarios")
    usuarios=cursor.fetchall()
    print("LO QUE TENEMOS EN BASE DE DATOS")
    for usuario in usuarios:
        
        print("ID:", usuario[0])
        print("name:", usuario[1])
        print("email:", usuario[2])
        print("psw:", usuario[3])
        print("tipo:", usuario[4])

        print("\n")

    #  commitear para guardar cambios
    conexion.commit()
    #cerrar conexion
    conexion.close()    

#mostrar todos los archivos
def showAllFiles():
     #Conexión
    conexion=sqlite3.connect('/pruebaRaven.db')
    #crear cursor
    cursor = conexion.cursor()
    cursor.execute("SELECT * FROM archivos")
    print("LO QUE TENEMOS EN ARCHIVOS")
   
    result = cursor.fetchall()
   
    #  commitear para guardar cambios
    conexion.commit()
    
    #cerrar conexion
    conexion.close()  
    return result  

#Borrar todos los usuarios
def deleteAllUsers():
     #Conexión
    conexion=sqlite3.connect('/pruebaRaven.db')
    #crear cursor
    cursor = conexion.cursor()
   # PARA BORRAR
    cursor.execute("DELETE FROM usuarios")
    #  commitear para guardar cambios
    conexion.commit()
    #cerrar conexion
    conexion.close()

#Borrar todos los archivos
def deleteAllFiles():
     #Conexión
    conexion=sqlite3.connect('/pruebaRaven.db')
    #crear cursor
    cursor = conexion.cursor()
   # PARA BORRAR
    cursor.execute("DELETE FROM archivos")
    #  commitear para guardar cambios
    conexion.commit()
    #cerrar conexion
    conexion.close()

#Comprobacion del usuario que inicia sesion 
def checkUserToLog(name,passw):
    print("estamos en check")
     #Conexión
    conexion=sqlite3.connect('/pruebaRaven.db')
    #crear cursor
    cursor = conexion.cursor()
    cursor.execute("SELECT * FROM usuarios WHERE name= ? AND password = ? ",(name,passw,))
    result = cursor.fetchone()
    return result
    
    #  commitear para guardar cambios
    conexion.commit()
    #cerrar conexion
    conexion.close()
    
#Comprobar que el nombre no esta en uso
def checkUserName(name):
    print("estamos en check del name al registrar")
     #Conexión
    conexion=sqlite3.connect('/pruebaRaven.db')
    #crear cursor
    cursor = conexion.cursor()
    cursor.execute("SELECT * FROM usuarios WHERE name= ? ",(name,))
    result = cursor.fetchone()
    return result
    
    #  commitear para guardar cambios
    conexion.commit()
    #cerrar conexion
    conexion.close() 

#Recoger el user por id
def takeUserById(id):
    print("estamos en check del name al registrar")
     #Conexión
    conexion=sqlite3.connect('/pruebaRaven.db')
    #crear cursor
    cursor = conexion.cursor()
    cursor.execute("SELECT * FROM usuarios WHERE id= ? ",(id,))
    result = cursor.fetchone()
    return result
    
    #  commitear para guardar cambios
    conexion.commit()
    #cerrar conexion
    conexion.close()        

#Eliminar el fichero
def deleteFile(item_id):
    #Conexión
    conexion=sqlite3.connect('/pruebaRaven.db')
    #crear cursor
    cursor = conexion.cursor()
   # PARA BORRAR
    cursor.execute("DELETE FROM archivos WHERE id = ?",(item_id,))
    #  commitear para guardar cambios
    conexion.commit()
    #cerrar conexion
    conexion.close()