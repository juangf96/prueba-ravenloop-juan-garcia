from flask import Flask, render_template, redirect, url_for, request,flash
from flask_login import LoginManager, login_user, current_user, login_required, user_login_confirmed
from werkzeug.utils import secure_filename
from werkzeug.datastructures import FileStorage
from bbdd.bbdd import insertFile,showAllFiles,deleteAllFiles
import os
import datetime
import hashlib


def wsfiles(app):
    allFiles = showAllFiles()
    print(allFiles)
    typeUser = current_user[4]
    # deleteAllFiles()

    try:
        app.config['UPLOAD_FOLDER'] = './ArchivosPDF'
        app.config['CUSTOM_STATIC_PATH'] ='/ArchivosPDF'
        print("ESTAMOS EN LA SUBIDA DE FILES")
        if request.method == 'POST':
            f=request.files['archivo']
            filename = secure_filename(f.filename)
            f.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            print("ARCHIVO SUBIDO")
            idUser = current_user[0]
            
            sizeFile = os.stat("ArchivosPDF/"+filename).st_size
            sizeFile = sizeFile/1024
            dateFile = datetime.date.today()
            #CIFRADO DEL COCUMENTO
            document= open("ArchivosPDF/"+filename,"rb").read()
            h = hashlib.sha256(document)
            fileHash = h.hexdigest()
            print(fileHash)

            fileObject=(idUser,filename,sizeFile,dateFile,fileHash)
            print(fileObject)
            insertFile(fileObject)
            return redirect('/showfiles')
    except:        
        return redirect('/showfiles')

    

        
    print(allFiles)
    return render_template('showfiles.html',allFiles=allFiles,typeUser=typeUser)