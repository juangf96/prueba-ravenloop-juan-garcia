from flask import Flask, render_template, redirect, url_for, request,flash
from bbdd.bbdd import crearDataBase,insertUser,showAllUsers,deleteAllUsers,checkUserName
# from models import User
import hashlib

def wsregister():
    if request.method == 'POST':
        #recogida de datos
        try:
            name = request.form['name']
            email = request.form['email']
            psw = request.form['psw']
            pswRepeat = request.form['pswRepeat']
            userType = request.form.get('userType')
            checkname=checkUserName(name)
            if not checkname:
                checkname=""
            else:  
                checkname=checkname[1]  
            if  name != checkname:
                if psw == pswRepeat:
                    h= hashlib.sha256(b"{psw}")
                    #cifrado de contraseña
                    pswHash = h.hexdigest()
                    userObject= (name, email, pswHash, userType)
                    crearDataBase()
                    insertUser(userObject)
                    showAllUsers()
                    flash("EL USUARIO SE HA SUBIDO")
                    #deleteAllUsers()
                else:
                    flash("Contraseñas no coinciden")

            else: 
                flash("Nombre de usuario ya en uso, elija otro")
        except:
             return redirect('/')    
            

        
    return render_template('register.html')
