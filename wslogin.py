from flask import Flask, render_template, redirect, url_for, request,flash
from flask_login import LoginManager, login_user, current_user, login_required, user_login_confirmed
from bbdd.bbdd import checkUserToLog
import hashlib
from models import User

def wslogin():
    
    print("hemos entrado en wslogin")
    if request.method == 'POST':
        try:
            print("POST")
            name = request.form['name']
            psw = request.form['psw']
            #cifrado de contraseña
            h = hashlib.sha256(b"{psw}")
            pswHash = h.hexdigest()

            #recogida de datos, query a bbdd y si todo va bien, entramos
            userBbdd=checkUserToLog(name,pswHash)
            if not userBbdd:
                flash("Contraseña o usuario no existen")
                return redirect('/')
                flash("Contraseña o usuario no existen")
            else:    
                userModel = User(userBbdd[0],userBbdd[1],userBbdd[2],userBbdd[3],userBbdd[4])
                if(pswHash== userModel.password):
                    print(userModel)
                    #Usuario logueado por login_user
                    login_user(userModel,remember=False,duration=None)
                    return redirect('showfiles')
                else:
                    return redirect('/')
        except:
            return redirect('/')    

    return render_template('login.html')
