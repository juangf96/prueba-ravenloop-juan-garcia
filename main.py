from flask import Flask, render_template, redirect, url_for, send_from_directory,send_file
from flask_login import LoginManager, login_user, current_user,login_required,logout_user
from bbdd.bbdd import deleteFile,takeUserById
from wsregister import wsregister
from wslogin import wslogin
from wsfiles import wsfiles
from models import User

import os


app = Flask(__name__, template_folder='templates',static_folder='./ArchivosPDF/')
app.secret_key='clave_secreta_flask'
# app.config['UPLOAD_FOLDER'] =C:\SGDF'

    
login_manager = LoginManager(app)
login_manager.init_app(app)
@login_manager.user_loader
def user_loader(id):
    return takeUserById(id)

#Ruta principal
@app.route('/', methods = ['GET','POST'])
def loginRoute():    
    return wslogin()

#Ruta de registro
@app.route('/register', methods = ['GET','POST'])
def registerRoute(): 
    try:
        return wsregister()
    except:
        return wslogin()   

#Ruta de los archivos
@login_required
@app.route('/showfiles', methods = ['GET','POST'])
def filesRute(): 
    #PARA VER LOS ARCHIVOS Y SUBIRLOS
    try:
        global app
        global route
        if current_user[1]:
            return wsfiles(app)
        else:
            return wslogin()  

        return wslogin()  
    except:
        return redirect('/')          

#Ruta cerrado de sesion
@app.route("/logout")
def logout():
    logout_user()
    print(current_user)
    return redirect('/')    

#Ruta de descarga
@app.route('/downloadFile/<path:filename>', methods = ['GET','POST'])
def download(filename):
    # DESCARGA DEL FICHERO
    return send_file('ArchivosPDF/'+filename ,as_attachment=True)


# Ruta de borrado
@login_required
@app.route('/delete_item/<path:item_id>/<path:item_name>', methods=['GET', 'POST'])
def delete_item(item_id,item_name):
    if current_user[4]== "admin":
        #EL DELETE
        try:    
            deleteFile(item_id)
            os.remove(os.path.join('./ArchivosPDF/', item_name))
            return redirect('/showfiles')
        except:
            return redirect('/showfiles')    
    else:    
        return redirect('/showfiles')


if __name__=='__main__':
    app.run(port=5000, debug=True)

    
    